package com.example.pojo.Model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class Article {
    @NotNull(message = "{0}")
    Integer id;
    @NotEmpty(message = "{1}")
    String title;
    Category category;
    @NotEmpty(message ="{0}")
    String description;
    @NotEmpty(message = "{0}")
    String author,createdDate,thumbnail;

    public Article(@NotNull(message = "{0}") int id, @NotEmpty(message = "{1}") String title, Category category, @NotEmpty(message = "{0}") String description, @NotEmpty(message = "{0}") String author, @NotEmpty(message = "{0}") String createdDate, @NotEmpty(message = "{0}") String thumbnail) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.description = description;
        this.author = author;
        this.createdDate = createdDate;
        this.thumbnail = thumbnail;
    }

    public Integer getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", category=" + category +
                ", description='" + description + '\'' +
                ", author='" + author + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }
    public Article(){}
}

