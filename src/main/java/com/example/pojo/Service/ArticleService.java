package com.example.pojo.Service;

import com.example.pojo.Model.Article;

import java.util.List;

public interface ArticleService {
    List<Article>  getAll();
}
